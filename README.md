# repo-cloner

Utility to check git clone speed.

Repo cloner will clone requested repositories to a local machine and
will report amount of time required to sync each repository and also
total time for all repositories. List of repositories is provided via
yaml file. Report provided in json format.

## Installation

Repo cloner is a conventional golang utility, regular "go get" will
work on this repository:

```bash
$ go get -v ./...
```

## Usage

Repo cloner doesn't have mandatory arguments. By default it will try
to load list of repositories from the `repos.yml` file in a current
folder. General usage format:

```bash
$ repo-cloner [flags] [repos.yml]
```

Optional configuration flags:

* `-timeout`     - Number of seconds to wait before marking clone attempt as a failure. Defaults to 30.
* `-depth`       - Switches to shallow clone with specified amount of commits. <= 0 will perform full clone. Defaults to -1.
* `-destination` - Folder to clone repositories into. If not provided cloner will user user's home directory.
* `-out`         - Location of a report json file. If not provided report will be sent to stdout.

## YAML configuration format

```yaml
- git@github.com:skycoin/skycoin.git
- git@gitlab.com:ap4y/skycoin-node-sync-test.git
```

## Report's JSON schema

```golang
type Report struct {
	Duration    time.Duration `json:"duration"` // total duration across all checked repositories
	RepoResults []Result      `json:"results"`  // individual results for each repository
}

type Result struct {
	Repository string        `json:"repository"` // repository url
	Success    bool          `json:"success"`    // indicates that clone was successful
	Duration   time.Duration `json:"duration"`   // clone duration
}
```

Sample report
```json
{
  "duration": 12888642691,
  "results": [
    {
      "repository": "git@github.com:skycoin/skycoin.git",
      "success": true,
      "duration": 9564868569
    },
    {
      "repository": "git@gitlab.com:ap4y/skycoin-node-sync-test.git",
      "success": true,
      "duration": 12885405475
    }
  ]
}
```
